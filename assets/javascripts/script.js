$(function(){
	var body = $('body'),
		intro = $('.intro'),
		team = $('.team'),
		marker = $('.team-marker'),
		overlay = $('.overlay'),
		post = $('.post-holder').find('.post'),
		popupWrap = $('.popup-wrap'),
		popup = popupWrap.find('.popup'),
		popupClose = popup.find('.popup-close'),
		popupPrev = popup.find('.popup-prev'),
		popupNext = popup.find('.popup-next'),
		postCounter = 0,
		video = $('.video'),
		videoIcon = $('.video-icon');


	$('.scroll-down').click(function(event){
		event.preventDefault();
		$('body, html').animate({
			scrollTop: intro.outerHeight() + intro.offset().top + "px"
		},500)
	});


	post.click(function(){
		overlay.fadeIn();
		popupWrap.fadeIn();
		body.addClass('no-scroll');
		popup.find('.post').remove();
		popup.append($(this).clone());
		postCounter=$(this).index();
	});

	popupClose.click(function(){
		overlay.fadeOut();
		popupWrap.fadeOut();
		body.removeClass('no-scroll');
	});

	popupWrap.click(function(){
		popupWrap.fadeOut();
		overlay.fadeOut();
		body.removeClass('no-scroll');
	});

	popup.click(function(){
		return false
	});

	popupPrev.click(function(){
		if(postCounter==0){
			postCounter=post.length-1
		}else{
			postCounter--;			
		}
		popup.find('.post').remove();
		popup.append(post.eq(postCounter).clone());
	});

	popupNext.click(function(){
		if(postCounter==post.length-1){
			postCounter=0
		}else{
			postCounter++;			
		}
		popup.find('.post').remove();
		popup.append(post.eq(postCounter).clone());
	});

	



	marker.click(function(){
		if($(this).hasClass('open')){
			$(this).removeClass('open');
		}else{
			marker.removeClass('open');
			$(this).addClass('open');
		}
		return false
	});

	team.click(function(){
		marker.removeClass('open');
	});



	videoIcon.click(function(event){
		event.preventDefault();
		video.addClass('open');
	});


});