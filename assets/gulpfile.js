var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    compass = require('gulp-compass'),
    sourcemaps = require('gulp-sourcemaps'),
    paths = {
        // All dirs and files: 'javascripts/**/*.js', Exclude: 'javascripts/ie/*.js'
        javascripts: [
            'javascripts/vendor/*.js', 'javascripts/helpers/*.js', 'javascripts/app/*.js',
            '!javascripts/scripts.min.js'
        ],
        images: 'images/*.{png,jpg,gif}',
        test: ['gulpfile.js', 'javascripts/helpers/*.js', 'javascripts/app/**/*.js'], // Add all YOUR scripts
        sass: 'sass/**/*.{sass,scss}'
    };

gulp.task('minify', function () {
    gulp.src(paths.javascripts)
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('javascripts'));
});

gulp.task('image', function () {
    gulp.src(paths.images)
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest('images'));
});

gulp.task('test', function () {
    gulp.src(paths.test)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('compass', function () {
    gulp.src(paths.sass)
        .pipe(compass({
            config_file: 'config.rb',
            css: 'stylesheets'
        }))
        .pipe(gulp.dest('stylesheets'));
});

gulp.task('watch', function() {
    gulp.watch(paths.javascripts, ['minify']);
    gulp.watch(paths.sass, ['compass']);
});

gulp.task('default', ['minify', 'image']);